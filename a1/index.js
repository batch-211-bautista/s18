// console.log('Hello World!');

/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function

	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.

	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.

	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	

	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/


// Addition Function
function addNum(num1, num2) {
	console.log('Displayed sum of ' + num1 + ' and ' + num2);
	console.log(num1 + num2);
};

addNum(5, 15);
// End of Addition Function



// Subtraction Function
function subNum(num1, num2) {
	console.log('Displayed difference of ' + num1 + ' and ' + num2);
	console.log(num1 - num2);
};

subNum(20, 5);
// End of Subtraction Function



// Multiplication Function
function multiplyNum(num1, num2) {
	return num1 * num2;
};

let mul1 = 50;
let mul2 = 10;
let product = multiplyNum(mul1, mul2);
console.log('The product of ' + mul1 + ' and ' + mul2 + ':');
console.log(product);
// End of Multiplication Function



// Division Function
function divideNum(num1, num2) {
	return num1 / num2;
};

let div1 = 50;
let div2 = 10;
let quotient = divideNum(div1, div2);
console.log('The quotient of ' + div1 + ' and ' + div2 + ':');
console.log(quotient);
// End of Division Function



// Area of a circle function
function getCircleArea(r) {
	let area = 3.1416 * (r ** 2);
	return area;
};

let radius = 15;
let circleArea = getCircleArea(radius);
console.log('The result of getting the area of a circle with ' + radius + ' radius:');
console.log(circleArea);
// End of Area of a circle function



// Average of four numbers function
function getAverage(num1, num2, num3, num4) {
	let average = (num1 + num2 + num3 + num4) / 4;
	return average;
};

let av1 = 20;
let av2 = 40;
let av3 = 60;
let av4 = 80;
let averageVar = getAverage(av1, av2, av3, av4);
console.log('The average of ' + av1 + ',' + av2 + ',' + av3 + ' and ' + av4 + ':');
console.log(averageVar);
// End of Average of four numbers function



// Passing score function
function checkedIfpassed(score, totalScore) {
	let isPassed = (score / totalScore) * 100;
	isPassed = isPassed >= 75;
	return isPassed;
};

let score = 38;
let totalScore = 50;
let isPassingScore = checkedIfpassed(score, totalScore);
console.log('Is ' + score + '/' + totalScore + ' a passing score?');
console.log(isPassingScore);
// End of Passing score function