// console.log('Hello World');

// Functions

	// Parameters and Arguments


		function printInput() {
			let nickname = prompt('Enter your nickname: ');
			console.log('Hi, ' + nickname);
		};

		// printInput();

		// However, for other cases, functions can also process data directly passed into it instead of relying only on global variables and prompt()
		function printName(name) {
			console.log('My name is ' + name);
		};

		printName('Juana');

		// You can directly pass data in to the function
		// the function can then call/use that data which is referred as "name" within our function
		// "name" is called a parameter
		// a "parameter" acts as a named variable/container that exists only inside of a function
		// It is used to store information that is provided to a function when it is called or invoked

		// "Juana", the information/data provided directly into a function is called an argument

		// in the following examples, "John" and "Jane" are both arguments since both of them are supplied as information that will be used to print out the full message

		printName("John");
		printName("Jane");

		// variables can also be passed as an argument
		let sampleVariable = 'Yui';
		printName(sampleVariable);

		// Function arguments cannot be used by a function if there are no parameters provided within the function.

		function checkDivisibilityBy8(num) {
			let remainder = num % 8;
			console.log('The remainder of ' + num + ' divided by 8 is: ' + remainder);

			let isDivisibleBy8 = remainder === 0;
			console.log('Is ' + num + ' divisible by 8?');
			console.log(isDivisibleBy8);
		};

		checkDivisibilityBy8(64);
		checkDivisibilityBy8(28);

		function checkDivisibilityBy4(num) {
			let remainder = num % 4;
			console.log('The remainder of ' + num + ' divided by 4 is: ' + remainder);

			let isDivisibleBy4 = remainder === 0;
			console.log('Is ' + num + ' divisible by 4?');
			console.log(isDivisibleBy4);
		};

		checkDivisibilityBy4(56);
		checkDivisibilityBy4(95);

		// you can also do the same using prompt(), however, take note that prompt() outputs a string. Strings are not ideal for mathematical computations

		// Functions as arguments
		// Function parameters can also accept other functions as arguments
		// Some complex functions use other functions as arguments to perform more complicated results
		// This will be further seen when we discuss array methods

		function argumentFunction() {
			console.log('This function was passed as an argument before the message was printed');
		};

		function invokeFunction(argumentFunction) {
			argumentFunction();
		};

		// Adding and removing the parentheses "()" impacts the output of JavaScript heavily
		// When a function is used with parentheses "()", it denotes invoking/calling a function

		invokeFunction(argumentFunction);
		// a function used without a parenthesis is normally associated with using the function as an argument to another function

		console.log(argumentFunction);
		// or finding more information about a function in the console using console.log()

		// Using multiple parameters
		// multiple "arguments" will correspond to the number of "parameters" declared in a function in suceeding order

		function createFullName(firstName, middleName, lastName) {
			console.log(firstName + ' ' + middleName + ' ' + lastName);
		};

		createFullName('Juan', 'Dela', 'Cruz');

		// In JS, providing more/less arguments than the expected parameters will NOT return an error

		createFullName('Juan', 'Dela');
		createFullName('Jane', 'Dela', 'Cruz', 'Hello');

		// we can use variables as arguments
		let firstName = 'John';
		let middleName = 'Doe';
		let lastName = 'Smith';

		createFullName(firstName, middleName, lastName);

		function printFullName(middleName, firstName, lastName) {
			console.log(firstName + " " + middleName + " " + lastName);
		};
		printFullName('Juan', 'Dela', 'Cruz');
		// results to "Dela Juan Cruz" because "Juan" was received as middleName, "Dela" was received as firstName.

		function printFriends(f1, f2, f3) {
			console.log('My three friends are: ' + f1 + ', ' + f2 + ', ' + f3);
		};

		printFriends('Juan', 'Mario', 'Jose');

		// The Return Statement

		// The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function

		function returnFullName(firstName, middleName, lastName) {
			// console.log(firstName + ' ' + middleName + ' ' + lastName);
			return firstName + ' ' + middleName + ' ' + lastName;
			console.log('This message will not be printed');
			// notice that the console.log after the return is no longer printed in the console...
			// that is because ideally any line/block of code that comes after the return statement is ignored because it ends the function execution
		};

		// in our example, the "return" statement allows us to output a value of a function to be passed to a line/block of code that called the function
		// the "returnFullName" function was called in the same line as declaring a variable

		// whatever value is returned from "returnFullName" function is stored in the "completeName" variable

		let completeName = returnFullName('Jeffrey', 'Smith', 'Bezos');
		console.log(completeName);

		let completeName2 = returnFullName('Nehemiah', 'C.', 'Ellorico');
		console.log(completeName2);

		let combination = completeName + completeName2;
		console.log(combination);

		// this way, a function is able to return a value we can further use or manipulate in our program instead of only printing/displaying it in the console

		console.log(returnFullName(firstName, middleName, lastName));
		// in this example, console.log() will print the returned value of the returnFullName() function

		function returnAddress(city, country) {
			let fullAddress = city + ', ' + country;
			return fullAddress;
		};

		let myAddress = returnAddress('Cebu City', 'Philippines');
		console.log(myAddress);

		function printPlayerInfo(username, level, job) {
			// console.log('username: ' + username);
			// console.log('level: ' + level);
			// console.log('Job: ' + job);
			return('username: ' + username + ' level: ' + level + ' Job: ' + job);
		};

		let user1 = printPlayerInfo('knight_white', 95, 'Paladin');
		console.log(user1);

		function multiplyNumbers(num1, num2){
			return num1 * num2;
		};

		let mul1 = 5;
		let mul2 = 9;
		let product = multiplyNumbers(mul1, mul2);
		console.log('The product of ' + mul1 + ' and ' + mul2 + ':');
		console.log(product);